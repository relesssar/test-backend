<?php

namespace Tests\Unit;

use App\Models\MerchantUser;
use PHPUnit\Framework\TestCase;
use App\Http\Controllers\GatewayController;
use Mockery;
use App\Models\Payment;


use Illuminate\Support\Facades\DB;



class СheckDailyLimitTest extends TestCase
{

    function test_checkDailyLimitModel()
    {

        $productsData = [
            ['merchant_id' => 6, 'payment_id' => 1]
        ];

        $mockPayment = Mockery::mock(Payment::class)->makePartial();
        $mockPayment->shouldReceive('getDailyLimit')->andReturnSelf();
        $mockPayment->shouldReceive('get')->andReturn(collect($productsData));

        $getPayment = $mockPayment->getDailyLimit(7)->get();


        $this->assertTrue(true);
    }

    function test_checkDailyLimit()
    {
        $mockPayment = $this->getMockBuilder(Payment::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mockMerch = $this->getMockBuilder(MerchantUser::class)
            ->disableOriginalConstructor()
            ->getMock();

        $expectedData = 'expected data';

        $mockPayment->expects($this->once())
            ->method('getDailyLimit')
            ->with(7)
            ->willReturn($expectedData);

        $testController = new GatewayController($mockPayment,$mockMerch);

        $userData = $testController->checkDailyLimit(7);

        $this->assertEqualsCanonicalizing($expectedData, $userData);

    }
}

<?php

namespace App\Console\Commands;

use App\Http\Controllers\Gate6Controller;
use App\Http\Controllers\Gate816Controller;
use App\Models\MerchantUser;
use Illuminate\Console\Command;
use App\Models\Payment;

class sendNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gateway:sendnotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification to callback services';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * Send notification to callback services
     * @return int
     */
    public function handle()
    {
        $get_failed = new Payment();
        $get_failed_arr = $get_failed->getFailedPayments();

        foreach ($get_failed_arr as $payment) {
            // Notication gate
            var_dump($payment);
            if ($payment['merchant_id'] == 6) {
                $gate = new Gate6Controller(new Payment(), new MerchantUser(), true);
                $gate->sendPayment($payment['merchant_id'],$payment['payment_id']);
            } elseif ($payment['merchant_id'] == 816) {
                $gate = new Gate816Controller(new Payment(), new MerchantUser(), true);
                $gate->sendPayment($payment['merchant_id'],$payment['payment_id']);
            }
        }

        return 0;
    }
}

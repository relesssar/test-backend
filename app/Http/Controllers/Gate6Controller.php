<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Gate6Controller extends GatewayController
{
    /** Send notification to callback url
     * @param $merchant_id
     * @param $payment_id
     * @param bool $is_force
     * @return bool
     */
    public function sendPayment($merchant_id, $payment_id, $is_force = false)
    {
        if (($this->isProccedNotification($merchant_id,$payment_id) or $is_force)){

            $url = $this->getMerchCallbackurl($merchant_id);

            $data = $this->payment::where('payment_id','=',$payment_id)->get()->take(1);
            $merchant = $this->merchant::where('merchant_id','=',$merchant_id)->get()->take(1);

            $post_data = [
                'merchant_id'=>$data[0]['merchant_id'],
                'payment_id'=>$data[0]['payment_id'],
                'status'=>$data[0]['status'],
                'amount'=>$data[0]['amount'],
                'amount_paid'=>$data[0]['amount_paid'],
                'timestamp'=>strtotime($data[0]['created_at'])
            ];

            $post_data_sorted = array_merge([],$post_data);
            ksort($post_data_sorted );

            $hash_str = implode(':', $post_data_sorted);
            $hash_str .= $merchant[0]['merchant_key'];

            // Example hash sorted by key
            // string '6:13:500:500:1654103837:completedKaTf5tZYHx4v7pgZ';
            // hash   'f027612e0e6cb321ca161de060237eeb97e46000da39d3add08d09074f931728'

            $sign = hash('sha256', $hash_str);
            $sign_filed=['sign'=>$sign];
            $final_array=array_merge($post_data, $sign_filed);
            $post_json = json_encode($final_array);

            $httpcode='';
            try {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_HTTPHEADER, [
                    'Content-Type: application/json',
                ]);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post_json);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

                $response = curl_exec($ch);
                $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                curl_close($ch);
            } catch (\Exception $exception) {
                // Log
            }

            if ($httpcode == 200) {
                // Success
                $this->successDelivered($payment_id);
            } else {
                // Fail
            }

        }

        return true;
    }
}

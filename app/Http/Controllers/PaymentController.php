<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\MerchantUser;
use App\Http\Controllers\Gate6Controller;

class PaymentController extends Controller
{
    /*
     * Test dumb api for incoming payments
     * Body:
     *     "merchant_id"
     *     "payment_id"
     *     "status"
     *     "amount"
     *     "amount_paid"
     */
    public function pay(Request $request) {

        $postbody='';
        // Check for presence of a body in the request
        if (count($request->json()->all())) {
            $postbody = $request->json()->all();
        }

        $get_exist_payment = Payment::where('payment_id', $postbody['payment_id'])->first();

        // if payment exist or new
        if ($get_exist_payment == null ) {
            // new payment
            $new_payment = new Payment();
            $new_payment->merchant_id = $postbody['merchant_id'];
            $new_payment->payment_id =  $postbody['payment_id'];
            $new_payment->status =      $postbody['status'];
            $new_payment->amount =      $postbody['amount'];
            $new_payment->amount_paid = $postbody['amount_paid'];
            $new_payment->save();

            // Notication gate
            if ($postbody['merchant_id'] == 6) {
                $gate = new Gate6Controller(new Payment(), new MerchantUser());
                $gate->sendPayment($postbody['merchant_id'],$postbody['payment_id']);
            } elseif ($postbody['merchant_id'] == 816) {
                $gate = new Gate816Controller(new Payment(), new MerchantUser());
                $gate->sendPayment($postbody['merchant_id'],$postbody['payment_id']);
            }

            return response()->json([
                'status' => 'OK',
                'message'=>'New payment successfully accepted'
            ], 200);
        } else {

            // Check if status changed
            if ($get_exist_payment->status == $postbody['status']) {
                return response()->json([
                    'status' => 'OK',
                    'message'=>'Payment with no change'
                ], 200);
            } else {
                // modify payment
                $get_exist_payment->status = $postbody['status'];
                $get_exist_payment->save();

                // Notication gate
                if ($postbody['merchant_id'] == 6) {
                    $gate = new Gate6Controller(new Payment(), new MerchantUser());
                    $gate->sendPayment($postbody['merchant_id'],$postbody['payment_id']);
                } elseif ($postbody['merchant_id'] == 816) {
                    $gate = new Gate816Controller(new Payment(), new MerchantUser());
                    $gate->sendPayment($postbody['merchant_id'],$postbody['payment_id']);
                }

                return response()->json([
                    'status' => 'OK',
                    'message'=>'Payment successfully changed'
                ], 200);
            }
        }
    }
}

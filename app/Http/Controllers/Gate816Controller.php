<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Gate816Controller extends GatewayController
{
    /** Send notification to callback url
     * @param $merchant_id
     * @param $payment_id
     * @param bool $is_force
     */
    public function sendPayment($merchant_id, $payment_id, $is_force = false) {
        if (($this->isProccedNotification($merchant_id,$payment_id) or $is_force)){
            $url = $this->getMerchCallbackurl($merchant_id);

            $data = $this->payment::where('payment_id','=',$payment_id)->get()->take(1);
            $merchant = $this->merchant::where('merchant_id','=',$merchant_id)->get()->take(1);

            // Random string
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < 8; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

            $post_data = [
                'project'=>$data[0]['merchant_id'],
                'invoice'=>$data[0]['payment_id'],
                'status'=>$data[0]['status'],
                'amount'=>$data[0]['amount'],
                'amount_paid'=>$data[0]['amount_paid'],
                'rand' => $randomString
            ];

            $post_data_sorted = array_merge([],$post_data);
            ksort($post_data_sorted );

            $hash_str = implode('.', $post_data_sorted);
            $hash_str .= $merchant[0]['merchant_key'];

            // Example hash sorted by key
            // string '700.700.73.816.SNuHufEJ.completedrTaasVHeteGbhwBx'
            // hash   'd84eb9036bfc2fa7f46727f101c73c73'

            $sign = md5( $hash_str);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization: '.$sign,
            ]);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            $response = curl_exec($ch);
            $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            curl_close($ch);

            if ($httpcode == 200) {
                // Success
                $this->successDelivered($payment_id);
            } else {
                // Fail
            }

        }
    }
}

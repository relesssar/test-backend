<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\MerchantUser;

class GatewayController extends Controller
{
    protected $payment;
    protected $merchant;

    /**
     * GatewayController constructor.
     * @param Payment $payment
     * @param MerchantUser $merchant
     */
    function __construct(Payment $payment, MerchantUser $merchant) {
        $this->payment = $payment;
        $this->merchant = $merchant;
    }

    /** Get Merchant config limit
     * @param $merchant_id
     * @return mixed
     */
    function getMerchLimit($merchant_id) {
        $get_limit = $this->merchant->getMerchantLimit($merchant_id);
        return $get_limit;
    }


    /** Get used limit by today's date
     * @param $merchant_id
     * @return int
     */
    function checkDailyLimit($merchant_id) {
        $get_payment = $this->payment->getDailyLimit($merchant_id);
        return $get_payment;
    }

    /** Get payment by id
     * @param $payment_id
     * @return mixed
     */
    function getPayById($payment_id) {
        $get_pay = $this->payment->getPaymentById($payment_id);
        return $get_pay;
    }

    /** Check merchant limit config with daily limit
     * @param $merchant_id
     * @param $payment_id
     * @return bool
     */
    function isProccedNotification($merchant_id, $payment_id){
        $get_merch_limit = $this->getMerchLimit($merchant_id);
        $current_limit = $this->checkDailyLimit($merchant_id);

        if ($current_limit <= $get_merch_limit) {
            return true;
        } else {
            return false;
        }
    }

    /** Set is_delivered to 1
     * @param $payment_id
     */
    function successDelivered ($payment_id) {
        $payment = $this->payment::where('payment_id',$payment_id)->first();
        $payment->is_delivered = 1;
        $payment->save();
    }

    /** Get merchant callback url
     * @param $merchant_id
     * @return mixed
     */
    function getMerchCallbackurl($merchant_id) {
        $get_url = $this->merchant->getMerchantCallbackurl($merchant_id);
        return $get_url;
    }
}

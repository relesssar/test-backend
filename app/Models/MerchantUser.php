<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MerchantUser extends Model
{
    use HasFactory;

    /** Get Merchant config limit
     * @param $merchant_id
     * @return mixed
     */
    public function getMerchantLimit ($merchant_id) {
        $get_limit =  $this::where('merchant_id','=',$merchant_id)
            ->get()
            ->take(1)
            ->toArray();
        return $get_limit[0]['limit'];
    }

    /** Get merchant callback url
     * @param $merchant_id
     * @return mixed
     */
    public function getMerchantCallbackurl($merchant_id) {
        $url = $this::where('merchant_id','=',$merchant_id)
            ->get()
            ->take(1)
            ->toArray();
        return $url[0]['callback_url'];
    }
}

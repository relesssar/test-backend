<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;

    /** Get used limit by today's date
     * @param $merchant_id
     * @return int
     */
    public function getDailyLimit ($merchant_id) {
        $getCount = $this::whereDate('created_at', date('Y-m-d'))
            ->where('merchant_id', $merchant_id)
            ->get()
            ->toArray();
        return count($getCount);
    }

    /** Get payment by id
     * @param $payment_id
     * @return mixed
     */
    public function getPaymentById($payment_id) {
        $getpayment = $this::where('payment_id','=',$payment_id)
            ->get();
        return $getpayment;
    }

    /** Get all not delivered payments
     * @return mixed
     */
    function getFailedPayments () {
        $get_failed_payments = Payment::where('is_delivered', '=', 0)
            ->get()
            ->toArray();
        return $get_failed_payments;
    }
}

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\GatewayController;
use App\Http\Controllers\Gate6Controller;
use App\Models\MerchantUser;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/test', function() {
    //return GatewayController::checkDailyLimit(6);
    return response()->json(['status' => 'OK'], 200);
});


Route::post('/payment', [PaymentController::class,'pay']);